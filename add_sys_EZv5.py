import fitsio
from nbodykit.lab import *
from nbodykit import setup_logging, style
from nbodykit.source.catalog import FITSCatalog, BigFileCatalog, CSVCatalog
from nbodykit.transform import StackColumns, ConcatenateSources
from nbodykit.algorithms.fibercollisions import FiberCollisions
import numpy as np
from collections import Counter

import pymangle
import healpy as hp
from astropy.table import Table
from astropy.table import unique

import os
import sys

twopcfdir = '/global/homes/o/octobers/Projects/ebossDR16/twopcf/'
ezv5dir = '/global/cscratch1/sd/octobers/Projects/Simulation/EZmock/QSO_v5/'
ezv5dir_w = '/global/cscratch1/sd/octobers/Projects/Simulation/EZmock/QSO_v5_wcp_wnoz/'
ezv5dir_t = '/global/cscratch1/sd/octobers/Projects/Simulation/EZmock/QSO_v5_tiled/'
sys.path.insert(0, '/global/homes/o/octobers/Projects/PY_Codes')
from tpcf_tool import getstat, transformstat

def calc_im2_fraction_in_secctor(ihemi):
    dat16v5_full = FITSCatalog(os.path.join('/global/cscratch1/sd/octobers/Projects/eBOSSDR16/QSOcatalogues/cat_origin/', 
                                                'eBOSS_QSO_full_%s_v5.dat.fits')%(ihemi))
    sel = (((dat16v5_full['IMATCH'] == 1) | (dat16v5_full['IMATCH'] == 2)) & 
           (dat16v5_full['COMP_BOSS']>0.5) & (dat16v5_full['Z'] > 0.8) & (dat16v5_full['Z'] < 2.2) & 
           (dat16v5_full['sector_SSR'] > 0.5))
    data16v5_sel = dat16v5_full[sel]
    print('> select data done!')
    sec_key_in_data = np.array(list(Counter(data16v5_sel['SECTOR'].compute()).keys()))
    im2_sec_frac = -1 * np.ones(len(sec_key_in_data))
    ratio_global = data16v5_sel[data16v5_sel['IMATCH']==2].csize/data16v5_sel.csize
    print('fraction of im2 =', ratio_global)

    for isec in range(0, len(sec_key_in_data)):
        if np.mod(isec, 300)==0: print(isec)
        mask = np.where(data16v5_sel['SECTOR'] == isec)[0]
        if len(mask) > 0:
            fsel = data16v5_sel[mask]
            im2_sec_frac[isec] = len(fsel[fsel['IMATCH']==2])/len(fsel)

    im2_sec_frac_sel = im2_sec_frac[im2_sec_frac>0]
    sec_key_in_data_sel = sec_key_in_data[im2_sec_frac>0]
    im2_sec_frac_tab = np.zeros([int(len(im2_sec_frac_sel)+1), 2])
    im2_sec_frac_tab[0,0] = ratio_global    
    im2_sec_frac_tab[1:,0] = sec_key_in_data_sel
    im2_sec_frac_tab[1:,1] = im2_sec_frac_sel
    np.savetxt('/global/homes/o/octobers/Projects/ebossDR16/spectro/im2_sec_frac_qso_v5_%s.txt'%(ihemi.lower()), 
               im2_sec_frac_tab, fmt='%d %1.3f')
    print('save im2 file to', '/global/homes/o/octobers/Projects/ebossDR16/spectro/im2_sec_frac_qso_v5_%s.txt'%(ihemi.lower()))
    return ratio_global, im2_sec_frac_tab


def apply_im2_downsampling(imock, ihemi, catalog, outfile):
    POLYGON_BASE = '/global/cscratch1/sd/octobers/Projects/eBOSSDR16/sandbox/lss/collate/eboss_geometry_eboss0_eboss27'
    mply = POLYGON_BASE + '.ply'
    mfits = POLYGON_BASE + '.fits'
    m = pymangle.Mangle(mply)
    sector = Table.read(mfits, format='fits', hdu=1)['SECTOR']
    geo = '/global/cscratch1/sd/octobers/Projects/eBOSSDR16/sandbox/lss/collate/eBOSS_QSOgeometry_v5.fits'
    geometry = Table.read(geo, format='fits', hdu=1)['SECTOR','COMP_BOSS','SPEC_SUCCESS_RATE']
    geometry = unique(geometry)
    catalog_zsel = catalog[(catalog['Z'] > 0.8) & (catalog['Z'] < 2.2)]

    if np.mod(int(imock-1), 20)==0: print('> Getting sector of inputs')
    ids = m.polyid(catalog_zsel['RA'], catalog_zsel['DEC']) # get a polyid for each object
    ids = ids[(ids != -1)]
    catalog_sel = catalog_zsel[(ids != -1)]
    secs = sector[ids] # this is equivalent for i in ids, where(sector==ids[ii])

    if np.mod(int(imock-1), 20)==0: print("> Identify imatch==2 objects")
    sec_key_in_data = []
    im2_sec_frac = []
    try:
        with open('/global/homes/o/octobers/Projects/ebossDR16/spectro/im2_sec_frac_qso_v5_%s.txt'%(ihemi.lower()), 'r') as f:
            lines = f.readlines()
            iline = 0
            for line in lines:
                if iline == 0:
                    ratio_global = np.float(line.split()[0])
                    iline += 1
                else:
                    sec_key_in_data.append(np.int(line.split()[0]))
                    im2_sec_frac.append(np.float(line.split()[1])) 
        sec_key_in_data = np.hstack(sec_key_in_data)
        im2_sec_frac = np.hstack(im2_sec_frac)
    except:
        ratio_global, im2_sec_frac_tab = calc_im2_fraction_in_secctor(ihemi)
        sec_key_in_data = im2_sec_frac_tab[:,0]
        im2_sec_frac = im2_sec_frac_tab[:,1]
    sec_key_in_data_sel = sec_key_in_data[im2_sec_frac>0]

    im2_sector_frac = np.zeros(len(secs))
    for ii in range(len(sec_key_in_data_sel)):
        mask = np.where(secs == sec_key_in_data_sel[ii])[0]
        im2_sector_frac[mask] = im2_sec_frac[ii]
    im2_prob = im2_sector_frac[ids]
    im2_id = np.arange(catalog_sel.csize)
    sel_im2 = (im2_prob>0) & (im2_prob >ratio_global*np.random.rand(len(im2_prob)))
    im2_id = im2_id[sel_im2]
    print('fraction of imatch2 object = ', len(im2_id)/(catalog_sel.csize))

    if np.mod(int(imock-1), 20)==0: print('> Select objects (ssr>0.5) & (comp>0.5) ')
    idx = np.where(secs.reshape(secs.size,1)==geometry['SECTOR'])[1]
    ssr = geometry['SPEC_SUCCESS_RATE'][idx]
    probs = geometry['COMP_BOSS'][idx]
    sel0 = (ssr>0.5) & (probs>0.5) 
    ssr_im1 = ssr[sel0 & (~sel_im2)]
    probs_im1 = probs[sel0 & (~sel_im2)]
    probs_im2 = probs[sel0 & sel_im2]

    print('> Down sampling inputs')
    np.random.seed(1234)
    rands = np.random.random(size=len(probs_im1))
    catalog_sel_im1 = catalog_sel[sel0 & (~sel_im2)]
    wnoz_im1 = 1./ssr_im1
    catalog_sel_im1['WEIGHT_NOZ'] = wnoz_im1
    catalog_sel_im1['COMP'] = probs_im1
    catalog_sel_im1['IMATCH'] = 1.
#     catalog_sel_im1 = catalog_sel_im1[(probs_im1 > 0.5) & (probs_im1 > rands)]

    np.random.seed(1235)
    rands = np.random.random(size=len(probs_im2))
    catalog_sel_im2 = catalog_sel[sel0 & sel_im2]
    catalog_sel_im2['COMP'] = probs_im2
    catalog_sel_im2['WEIGHT_NOZ'] = 1.
    catalog_sel_im2['IMATCH'] = 2.
#     catalog_sel_im2 = catalog_sel_im2[(probs_im2 > 0.5) & (probs_im2 > rands)]

    cat = ConcatenateSources(catalog_sel_im1, catalog_sel_im2)
    print('>> concatenate source done!')
    cat.save(outfile)
    print('>> save file to', outfile)
    

def calc_wnoz_imatch_sector(imock, ihemi, catalog, outfile):
    POLYGON_BASE = '/global/cscratch1/sd/octobers/Projects/eBOSSDR16/sandbox/lss/collate/eboss_geometry_eboss0_eboss27'
    mply = POLYGON_BASE + '.ply'
    mfits = POLYGON_BASE + '.fits'
    m = pymangle.Mangle(mply)
    sector = Table.read(mfits, format='fits', hdu=1)['SECTOR']
    geo = '/global/cscratch1/sd/octobers/Projects/eBOSSDR16/sandbox/lss/collate/eBOSS_QSOgeometry_v5.fits'
    geometry = Table.read(geo, format='fits', hdu=1)['SECTOR','COMP_BOSS','SPEC_SUCCESS_RATE']
    geometry = unique(geometry)
    catalog_zsel = catalog[(catalog['Z'] > 0.8) & (catalog['Z'] < 2.2)]

    if np.mod(int(imock-1), 20)==0: print('> Getting sector of inputs')
    ids = m.polyid(catalog_zsel['RA'], catalog_zsel['DEC'])
    catalog_sel = catalog_zsel[(ids != -1)]
    ids = ids[(ids != -1)]
    secs = sector[ids] # this is equivalent for i in ids, where(sector==ids[ii])

    sec_key_in_data = []
    im_sec_frac = [] ## init imatch fraction of objects in a sector 
    try:
        with open('/global/homes/o/octobers/Projects/ebossDR16/spectro/im_sec_frac_qso_v5_%s.txt'%(ihemi.lower()), 'r') as f:
            lines = f.readlines()
            iline = 0
            for line in lines:
                if iline == 0:
                    ratio_global = np.array(list(map(float,line.split()[1:])))
                    iline += 1
                else:
                    sec_key_in_data.append(np.int(line.split()[0]))
                    im_sec_frac.append(np.array(list(map(float, (line.split()[1:]))))) 
        sec_key_in_data = np.vstack(sec_key_in_data)
        im_sec_frac = np.vstack(im_sec_frac) 
        im_keys_count = im_sec_frac.shape[1]
        if np.mod(int(imock-1), 20)==0: print("> load imatch objects from '/global/homes/o/octobers/Projects/ebossDR16/spectro/im_sec_frac_qso*")
    except:
        if np.mod(int(imock-1), 20)==0: print("> calculate imatch fraction for each sector--> careful this take very long!!!")
        ratio_global, im2_sec_frac_tab = calc_im_fraction_in_secctor(ihemi)
        sec_key_in_data = im2_sec_frac_tab[:,0]
        im_sec_frac = im2_sec_frac_tab[:,1]
    sec_key_in_data_sel = sec_key_in_data[im_sec_frac[:,0]>=0]

    im_sector_frac = np.zeros([catalog_sel.csize, im_keys_count])-999 ## number of objects in imock
    for ii in range(len(sec_key_in_data_sel)): 
        mask = np.where(secs == sec_key_in_data_sel[ii])[0]
        im_sector_frac[mask,:] = im_sec_frac[ii,:]

    im_key = []
    for ii in range(int(catalog_sel.csize)):
        if sum(im_sector_frac[ii,:]) < 0.5:
            im_key.append(-1)
        else:
            frac = im_sector_frac[ii,:].copy()
            frac = frac/frac.sum(axis=0,keepdims=1)
            im_key.append(np.random.choice(np.arange((im_sector_frac[0,:]).size)+1, p = frac))
    im_key = np.hstack(im_key)
    catalog_sel['IMATCH'] = im_key
    catalog_sel['SECTOR'] = secs
    
    if np.mod(int(imock-1), 20)==0: print('> assign wnoz for each sector')
    wnoz_sec = []
    wnoz = np.zeros(catalog_sel.csize)
    secs_unique = np.array(list(Counter(secs).keys()))
    for isec in range(int(secs_unique.size)):
        sel = (catalog_sel['SECTOR'].compute()==secs_unique[isec])
        cat = catalog_sel[sel]    
        n_total =  (len(np.where(cat['IMATCH']==1)[0])+len(np.where(cat['IMATCH']==4)[0])
                    +len(np.where(cat['IMATCH']==7)[0])+ len(np.where(cat['IMATCH']==9)[0]))
        n_success = (len(np.where(cat['IMATCH']==1)[0])+len(np.where(cat['IMATCH']==4)[0])+
                                                     len(np.where(cat['IMATCH']==9)[0]))
        if n_success >0:
            wnoz_sec.append(n_total/n_success)   
            wnoz[sel] = n_total/n_success * np.ones(cat.csize)
        else:
            wnoz_sec.append(-1)
            wnoz[sel] = -1 * np.ones(cat.csize)

    wnoz_sec = np.hstack(wnoz_sec)
    wnoz_s = wnoz.copy()
    im_sel2 = (catalog_sel['IMATCH']==2)
    wnoz_s[im_sel2] = 1.
    catalog_sel['WEIGHT_NOZ'] = wnoz_s

    idx = np.where(secs.reshape(secs.size,1)==geometry['SECTOR'])[1]
    ssr = geometry['SPEC_SUCCESS_RATE'][idx]
    comp = geometry['COMP_BOSS'][idx]
    catalog_sel['COMP'] = comp
    catalog_sel['sector_SSR'] = ssr
    catalog_sel['WEIGHT_CP'] = 1.
    sel0 = (ssr>0.5) & (comp>0.5) &(catalog_sel['WEIGHT_NOZ']>0)&(catalog_sel['WEIGHT_NOZ']<2) 

    cat_save = catalog_sel[sel0]
    cat_save.save(outfile)
    print('>> save file to', outfile)
    
def calc_wcp_fc_nearest_wnoz_sec(imock, catalog, im2file, outfile):

    POLYGON_BASE = '/global/cscratch1/sd/octobers/Projects/eBOSSDR16/sandbox/lss/collate/eboss_geometry_eboss0_eboss27'
    mply = POLYGON_BASE + '.ply'
    mfits = POLYGON_BASE + '.fits'
    m = pymangle.Mangle(mply)
    sector = Table.read(mfits, format='fits', hdu=1)['SECTOR']
    ntiles = Table.read(mfits, format='fits', hdu=1)['NTILES']
    geo = '/global/cscratch1/sd/octobers/Projects/eBOSSDR16/sandbox/lss/collate/eBOSS_QSOgeometry_v5.fits'
    geometry = Table.read(geo, format='fits', hdu=1)['SECTOR','COMP_BOSS','SPEC_SUCCESS_RATE']
    geometry = unique(geometry)
    zsel = (catalog['Z'] > 0.8) & (catalog['Z'] < 2.2)
    catalog_zsel = catalog[zsel]

    if np.mod(imock-1, 20)==0: print('> Getting sector of inputs')
    ids = m.polyid(catalog_zsel['RA'], catalog_zsel['DEC'])
    idx = (ids != -1)
    catalog_sel = catalog_zsel[idx]
    ids = ids[idx]
    secs = sector[ids] # this is equivalent for i in ids, where(sector==ids[ii])
    catalog_sel['SECTOR'] = secs
    
    if np.mod(imock-1, 20)==0: print('> Select objects (ssr>0.5) & (comp>0.5) ')
    idx = np.where(secs.reshape(secs.size,1)==geometry['SECTOR'])[1]
    ssr = geometry['SPEC_SUCCESS_RATE'][idx]
    probs = geometry['COMP_BOSS'][idx]
    catalog_sel['sector_SSR'] = ssr
    catalog_sel['COMP'] = probs
    sel0 = (catalog_sel['sector_SSR']>0.5) & (catalog_sel['COMP']>0.5) 
    catalog_sel = catalog_sel[sel0]

    
    if np.mod(imock-1, 20)==0: print("> Identify imatch==2 objects")
    sec_key_in_data = []
    im2_sec_frac = []
    with open(os.path.join('/global/homes/o/octobers/Projects/ebossDR16/spectro', im2file), 'r') as f:
        lines = f.readlines()
        iline = 0
        for line in lines:
            if iline == 0:
                ratio_global = np.float(line.split()[0])
                iline += 1
            else:
                sec_key_in_data.append(np.int(line.split()[0]))
                im2_sec_frac.append(np.float(line.split()[1]))        
    sec_key_in_data = np.hstack(sec_key_in_data)
    im2_sec_frac = np.hstack(im2_sec_frac)
    sec_key_in_data_sel = sec_key_in_data[im2_sec_frac>0]

    im2_sector_frac = np.zeros(len(secs))
    for ii in range(len(sec_key_in_data)):
        mask = np.where(secs == sec_key_in_data_sel[ii])[0]
        im2_sector_frac[mask] = im2_sec_frac[ii]
    im2_prob = im2_sector_frac[ids]
    im2_id = np.arange(catalog_sel.csize)
    sel_im2 = (im2_prob>0) & (im2_prob >ratio_global*np.random.rand(len(im2_prob)))
    im2_id = im2_id[sel_im2]
    print('fraction of imatch2 object = ', len(im2_id)/(catalog_sel.csize))

    imatch = np.ones(catalog_sel.csize)
    imatch[sel_im2] = 2
    catalog_sel['IMATCH'] = imatch
    catalog_im1 = catalog_sel[catalog_sel['IMATCH']==1]

    if np.mod(imock-1, 20)==0: print('> find the close pair')
    fc = FiberCollisions(catalog_im1['RA'], catalog_im1['DEC'], collision_radius=62./3600)
    idx_arr = np.arange(catalog_im1.csize)
    collision_global_label = (fc.labels['Collided'].compute()).astype('int')
    collided_sel = (fc.labels['Collided'] == 1) # get the global mask of the collided objects
    collided_idx = np.arange(catalog_im1.csize)[collided_sel] # set up the global index of the collided objects
    neighbour_id = fc.labels['NeighborID'][collided_idx] # get the neighber ids for the collided objects

    fc1 = FiberCollisions(catalog_im1['RA'], catalog_im1['DEC'], collision_radius=62./3600)   
    decollided_sel1 = np.where((fc1.labels['Collided'] == 0))[0]
    ind_tmp = idx_arr[decollided_sel1]
    collision_global_label[ind_tmp] = 0

    wcp_im1 = np.ones(catalog_im1.csize) # initiate close pair weight
    cp = fc.labels[collision_global_label == 1]
    cp_keys = np.array(list(Counter(cp['NeighborID'].compute()).keys()))
    cp_vals = np.array(list(Counter(cp['NeighborID'].compute()).values()))
    for ii in range(max(cp_vals), min(cp_vals)-1, -1):
        obj_id = cp_keys[(cp_vals == ii)]
        wcp_im1[obj_id] = ii + 1
    wcp_im1 = wcp_im1[collision_global_label == 0]    
    cat_nc_im1 = catalog_im1[collision_global_label == 0]  #     non-colliding group
    cat_cp_im1 = catalog_im1[collision_global_label == 1]  #     colliding group
    cat_nc_im1['WEIGHT_CP'] = wcp_im1

    if np.mod(imock-1, 20)==0: print('non-colliding data size', cat_nc_im1.csize)
    
    np.random.seed(1234)
    rands = np.random.random(size=cat_nc_im1.csize)
    ssr_im1 = cat_nc_im1['sector_SSR'].compute()
    cat_im1 = (cat_nc_im1[(ssr_im1>rands)]).copy()
    cat_im1['WEIGHT_NOZ'] = 1./cat_im1['sector_SSR'].compute()
    cat_im2 = catalog_sel[catalog_sel['IMATCH']==2]
    cat_im2['WEIGHT_NOZ'] = 1.
    cat_im2['WEIGHT_CP'] = 1.
    
    cat = ConcatenateSources(cat_im1, cat_im2)
    
    print('> Down sampling inputs')
    
    np.random.seed(1234)
    rands = np.random.random(size=cat.csize)
    comp = cat['COMP'].compute()
    mask = -1*np.ones(cat.csize)
    mask[(comp > 0.5) & (comp > rands)] = 1
    cat['MASK'] = mask

    cat.save(outfile)
    print(datetime.datetime.now(), 'done!')
    print('save file to', outfile)

    return ssr

def calc_wcp_fc_wnoz_sec(imock, catalog, im2file, outfile):

    POLYGON_BASE = '/global/cscratch1/sd/octobers/Projects/eBOSSDR16/sandbox/lss/collate/eboss_geometry_eboss0_eboss27'
    mply = POLYGON_BASE + '.ply'
    mfits = POLYGON_BASE + '.fits'
    m = pymangle.Mangle(mply)
    sector = Table.read(mfits, format='fits', hdu=1)['SECTOR']
    ntiles = Table.read(mfits, format='fits', hdu=1)['NTILES']
    geo = '/global/cscratch1/sd/octobers/Projects/eBOSSDR16/sandbox/lss/collate/eBOSS_QSOgeometry_v5.fits'
    geometry = Table.read(geo, format='fits', hdu=1)['SECTOR','COMP_BOSS','SPEC_SUCCESS_RATE']
    geometry = unique(geometry)
    zsel = (catalog['Z'] > 0.8) & (catalog['Z'] < 2.2)
    catalog_zsel = catalog[zsel]

    if np.mod(imock-1, 20)==0: print('> Getting sector of inputs')
    ids = m.polyid(catalog_zsel['RA'], catalog_zsel['DEC'])
    idx = (ids != -1)
    catalog_sel = catalog_zsel[idx]
    ids = ids[idx]
    secs = sector[ids] # this is equivalent for i in ids, where(sector==ids[ii])
    catalog_sel['SECTOR'] = secs
    
    if np.mod(imock-1, 20)==0: print('> Select objects (ssr>0.5) & (comp>0.5) ')
    idx = np.where(secs.reshape(secs.size,1)==geometry['SECTOR'])[1]
    ssr = geometry['SPEC_SUCCESS_RATE'][idx]
    probs = geometry['COMP_BOSS'][idx]
    catalog_sel['sector_SSR'] = ssr
    catalog_sel['COMP'] = probs
    sel0 = (catalog_sel['sector_SSR']>0.5) & (catalog_sel['COMP']>0.5) 
    catalog_sel = catalog_sel[sel0]

    
    if np.mod(imock-1, 20)==0: print("> Identify imatch==2 objects")
    sec_key_in_data = []
    im2_sec_frac = []
    with open(os.path.join('/global/homes/o/octobers/Projects/ebossDR16/spectro', im2file), 'r') as f:
        lines = f.readlines()
        iline = 0
        for line in lines:
            if iline == 0:
                ratio_global = np.float(line.split()[0])
                iline += 1
            else:
                sec_key_in_data.append(np.int(line.split()[0]))
                im2_sec_frac.append(np.float(line.split()[1]))        
    sec_key_in_data = np.hstack(sec_key_in_data)
    im2_sec_frac = np.hstack(im2_sec_frac)
    sec_key_in_data_sel = sec_key_in_data[im2_sec_frac>0]

    im2_sector_frac = np.zeros(len(secs))
    for ii in range(len(sec_key_in_data)):
        mask = np.where(secs == sec_key_in_data_sel[ii])[0]
        im2_sector_frac[mask] = im2_sec_frac[ii]
    im2_prob = im2_sector_frac[ids]
    im2_id = np.arange(catalog_sel.csize)
    sel_im2 = (im2_prob>0) & (im2_prob >ratio_global*np.random.rand(len(im2_prob)))
    im2_id = im2_id[sel_im2]
    print('fraction of imatch2 object = ', len(im2_id)/(catalog_sel.csize))

    imatch = np.ones(catalog_sel.csize)
    imatch[sel_im2] = 2
    catalog_sel['IMATCH'] = imatch
    catalog_im1 = catalog_sel[catalog_sel['IMATCH']==1]
    wcp = np.ones(catalog_im1.csize)
    wnoz = np.ones(catalog_im1.csize)
    
    
    if np.mod(imock-1, 20)==0: print('> find the close pair')
    fc = FiberCollisions(catalog_im1['RA'], catalog_im1['DEC'], collision_radius=62./3600)
    groupID_key = list(Counter(fc.labels['Label'][fc.labels['Label']>0].compute()).keys())
    catalog_im1['groupID'] = fc.labels['Label'].compute()
    catalog_im1['Collided'] = fc.labels['Collided'].compute()
    ## upweight according to the group_id: n_target/n_fiber
    ratio = []
    for ii in range(0, int(len(groupID_key))):
        sel1 = (catalog_im1['groupID'] == groupID_key[ii])
        num_nofiber = len(np.where(catalog_im1[sel1]['Collided'].compute()==1)[0])
        num_fiber = len(np.where(catalog_im1[sel1]['Collided'].compute()==0)[0])
        
        ratio.append(num_fiber/(num_fiber+num_nofiber))
        if num_fiber > 0:
            wcp[sel1] = (num_fiber+num_nofiber)/num_fiber
        else:
            print('groupID = ', ii, 'has no fiber assigned in the first iteration')
            print('>> enter fine searching .. ')
            ra_collide  = catalog_im1[catalog_im1['groupID'] == groupID_key[ii]]['RA'].compute()[0]
            dec_collide = catalog_im1[catalog_im1['groupID'] == groupID_key[ii]]['DEC'].compute()[0]
            for ifac in range(1,6):
                deg = int(ifac)*62/3600
                sel_sub = ((catalog_im1['RA']<ra_collide+deg)&(catalog_im1['RA']>ra_collide-deg)
                           &(catalog_im1['DEC']<dec_collide+deg)&(catalog_im1['DEC']>dec_collide-deg))
                num_fiber = int(catalog_im1[sel_sub].csize-np.count_nonzero(catalog_im1[sel_sub]['Collided'].compute()))
                if num_fiber>0:
                    wcp[sel_sub] = catalog_im1[sel_sub].csize/num_fiber
                    break
                else:
                    print('groupID = ', ii, 'fails to assign a fiber and is mistreated')

    catalog_im1['WEIGHT_CP'] = wcp
    catalog_im1 = catalog_im1[catalog_im1['Collided']==0]
        
        
    if np.mod(imock-1, 20)==0: print('non-colliding data size', catalog_im1.csize)

    np.random.seed(1234)
    rands = np.random.random(size=catalog_im1.csize)
    ssr_im1 = catalog_im1['sector_SSR'].compute()
    cat_im1 = (catalog_im1[(ssr_im1>rands)]).copy()
    cat_im1['WEIGHT_NOZ'] = 1./cat_im1['sector_SSR'].compute()
    
    cat_im2 = catalog_sel[catalog_sel['IMATCH']==2]
    cat_im2['WEIGHT_NOZ'] = 1.
    cat_im2['WEIGHT_CP'] = 1.
    cat_im2['groupID'] = -1
    cat_im2['Collided'] = -1
    
    cat = ConcatenateSources(cat_im1, cat_im2)
    
    print('> Down sampling inputs')
    
    np.random.seed(1234)
    rands = np.random.random(size=cat.csize)
    comp = cat['COMP'].compute()
    mask = -1*np.ones(cat.csize)
    mask[(comp > 0.5) & (comp > rands)] = 1
    cat['MASK'] = mask

    cat.save(outfile)
    print(datetime.datetime.now(), 'done!')
    print('save file to', outfile)

    return ssr
    
def calc_wcp_tiled_wnoz_sec(imock, catalog, outfile):
    POLYGON_BASE = '/global/cscratch1/sd/octobers/Projects/eBOSSDR16/sandbox/lss/collate/eboss_geometry_eboss0_eboss27'
    mply = POLYGON_BASE + '.ply'
    mfits = POLYGON_BASE + '.fits'
    m = pymangle.Mangle(mply)
    sector = Table.read(mfits, format='fits', hdu=1)['SECTOR']
    geo = '/global/cscratch1/sd/octobers/Projects/eBOSSDR16/sandbox/lss/collate/eBOSS_QSOgeometry_v5.fits'
    geometry = Table.read(geo, format='fits', hdu=1)['SECTOR','COMP_BOSS','SPEC_SUCCESS_RATE']
    geometry = unique(geometry)
    catalog_zsel = catalog[(catalog['Z'] > 0.8) & (catalog['Z'] < 2.2)]

    if np.mod(imock-1, 20)==0: print('> Getting sector of inputs')
    ids = m.polyid(catalog_zsel['RA'], catalog_zsel['DEC']) # get a polyid for each object
    ids = ids[(ids != -1)]
    catalog_sel = catalog_zsel[(ids != -1)]
    secs = sector[ids] # this is equivalent for i in ids, where(sector==ids[ii])
    secs_key = list(Counter(secs).keys())

    catalog_sel['SECTOR'] = secs
    
    idx = np.where(secs.reshape(secs.size,1)==geometry['SECTOR'])[1]
    ssr = geometry['SPEC_SUCCESS_RATE'][idx]
    probs = geometry['COMP_BOSS'][idx]
    catalog_sel['sector_SSR'] = ssr
    catalog_sel['COMP'] = probs
    
    catalog_im1 = catalog_sel[catalog_sel['IMATCH'] == 1]
    wcp = np.ones(catalog_im1.csize)
    wnoz = np.ones(catalog_im1.csize)

    if np.mod(imock-1, 20)==0: print("> Get objects in the same group ..")

    fc = FiberCollisions(catalog_im1['RA'], catalog_im1['DEC'], collision_radius=62./3600, seed=100)
    groupID_key = list(Counter(fc.labels['Label'][fc.labels['Label']>0].compute()).keys())
    catalog_im1['groupID'] = fc.labels['Label'].compute()

    ## upweight according to the group_id: n_target/n_fiber
    ratio = []
    for ii in range(0, int(len(groupID_key))):
        sel1 = (catalog_im1['groupID'] == groupID_key[ii])
        num_nofiber = len(np.where(catalog_im1[sel1]['FIBER'].compute()==0)[0])
        num_fiber = len(np.where(catalog_im1[sel1]['FIBER'].compute()==1)[0])
        ratio.append(num_fiber/(num_fiber+num_nofiber))
        if num_fiber > 0:
            wcp[sel1] = (num_fiber+num_nofiber)/num_fiber
        else:
            print('groupID = ', ii, 'has no fiber assigned in the first iteration')
            print('>> enter fine searching .. ')
            ra_collide  = catalog_im1[catalog_im1['groupID'] == groupID_key[ii]]['RA'].compute()[0]
            dec_collide = catalog_im1[catalog_im1['groupID'] == groupID_key[ii]]['DEC'].compute()[0]
            for ifac in range(1,6):
                deg = int(ifac)*62/3600
                sel_sub = ((catalog_im1['RA']<ra_collide+deg)&(catalog_im1['RA']>ra_collide-deg)
                           &(catalog_im1['DEC']<dec_collide+deg)&(catalog_im1['DEC']>dec_collide-deg))
                num_fiber = np.count_nonzero(catalog_im1[sel_sub]['FIBER'].compute())
                if num_fiber>0:
                    wcp[sel_sub] = catalog_im1[sel_sub].csize/num_fiber
                    break
                else:
                    print('groupID = ', ii, 'fails to assign a fiber and is mistreated')

    catalog_im1['WEIGHT_CP'] = wcp
    catalog_im1 = catalog_im1[catalog_im1['FIBER']==1]
            
    if np.mod(imock-1, 20)==0: print("> Downsample the redshift failure objects in sector ..")
    np.random.seed(1234)
    rands = np.random.random(size=catalog_im1.csize)
    ssr_im1 = catalog_im1['sector_SSR'].compute()
    cat_im1 = (catalog_im1[(ssr_im1>rands)]).copy()
    cat_im1['WEIGHT_NOZ'] = 1./cat_im1['sector_SSR'].compute()

    cat_im2 = catalog_sel[catalog_sel['IMATCH'] == 2]
    cat_im2['WEIGHT_CP'] = 1.
    cat_im2['WEIGHT_NOZ'] = 1.
    cat_im2['groupID'] = -1

    cat = ConcatenateSources(cat_im1, cat_im2)

    print('>> concatenate source done!')
    print('>> Downsample objects for completeness')
    rands = np.random.random(size=cat.csize)
    mask = -1 * np.ones(cat.csize)
    mask[(cat['COMP'] > 0.5) & (cat['COMP'] > rands)] = 1
    cat['MASK'] = mask

    cat.save(outfile)
    print('>> save file to', outfile)
    return cat

    

def gen_rand(cat_dat, cat_ran, is_downsample, imock, weight, dir_save, ffname):
    import bigfile
    if is_downsample:
        cat_dat = cat_dat[(cat_dat['MASK']==1)]
    idx_datacat = (np.random.choice(cat_dat.csize-1, size=int(cat_ran.csize), replace=True, p=None)).astype(int)

    weight = 'wnoz'
    if weight == 'wfiber':
        randcat_sel = np.empty(cat_ran.csize, dtype=[('RA', ('f8', 1)), ('DEC', ('f8',1)), ('Z', ('f8',1)), ('WEIGHT_FIBER', ('f8',1)),
                                                     ('WEIGHT_CP', ('f8',1)),
                                         ('COMP', ('f8',1)), ('WEIGHT_FKP', ('f8',1)), ('NZ', ('f8',1))])
    elif weight == 'wnoz':
        randcat_sel = np.empty(cat_ran.csize, dtype=[('RA', ('f8', 1)), ('DEC', ('f8',1)), ('Z', ('f8',1)), ('WEIGHT_NOZ', ('f8',1)),
                                                     ('WEIGHT_CP', ('f8',1)),
                                         ('COMP', ('f8',1)), ('WEIGHT_FKP', ('f8',1)), ('NZ', ('f8',1))])

    randcat_sel['RA'] = cat_ran['RA'].compute()
    randcat_sel['DEC'] = cat_ran['DEC'].compute()
    randcat_sel['Z'] = cat_ran['Z'].compute()[idx_datacat]
    if weight == 'wfiber':
        randcat_sel['WEIGHT_FIBER'] = cat_dat['WEIGHT_FIBER'].compute()[idx_datacat]
    elif weight == 'wnoz':    
        randcat_sel['WEIGHT_NOZ'] = cat_dat['WEIGHT_NOZ'].compute()[idx_datacat]
    randcat_sel['WEIGHT_CP'] = cat_dat['WEIGHT_CP'].compute()[idx_datacat]
    randcat_sel['COMP'] = cat_dat['COMP'].compute()[idx_datacat]
    randcat_sel['WEIGHT_FKP'] = cat_dat['WEIGHT_FKP'].compute()[idx_datacat]
    
    if weight == 'wfiber':
        name_space = ["RA", "DEC", "Z", "NZ", "COMP", "WEIGHT_CP", "WEIGHT_FIBER", "WEIGHT_FKP", ]
    elif weight == 'wnoz':
        name_space = ["RA", "DEC", "Z", "NZ", "COMP", "WEIGHT_CP", "WEIGHT_NOZ", "WEIGHT_FKP", ]

    with bigfile.BigFile(dir_save + ffname, create=True) as tmpff:
        for ii in range(len(name_space)):
            with tmpff.create(name_space[ii], dtype=('f8', 1), size = cat_ran.csize) as bb:
                bb.write(0, randcat_sel[name_space[ii]])
        
    print('write file to', dir_save+ffname)


