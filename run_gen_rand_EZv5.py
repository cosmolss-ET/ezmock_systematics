import os
import sys
from add_sys_EZv5 import gen_rand
from nbodykit.source.catalog import FITSCatalog, BigFileCatalog

ihemi = sys.argv[1]
istart = int(sys.argv[2])
iend = int(sys.argv[3])

cat_indir = '/global/cscratch1/sd/octobers/Projects/Simulation/EZmock/QSO_v5_wcp_wnoz/%s' %(ihemi.upper())
ran_save_dir = '/global/cscratch1/sd/octobers/Projects/Simulation/EZmock/QSO_v5_wcp_wnoz/'
ranv5 = FITSCatalog(os.path.join(ezv5dir, 'RANDOM/random_40x_eBOSS_QSO_%s_v5.fits')%(ihemi.upper()))

for imock in range(istart, iend):
    cat_dat = BigFileCatalog(os.path.join(cat_indir, 'EZmock_eBOSS_QSO_%s_v5_sec_fc_%04d.fits'%(ihemi.upper(), imock)))
    is_downsample = False
    if is_downsample:
        ffname = 'RANDOM/random_40x_eBOSS_QSO_%s_v5_sec_fc_downsample_%04d.fits'%(ihemi.upper(), imock)
    else:
        ffname = 'RANDOM/random_40x_eBOSS_QSO_%s_v5_sec_fc_%04d.fits'%(ihemi.upper(), imock)
    gen_rand(cat_dat=cat_dat, cat_ran=ranv5, is_downsample=is_downsample, imock=imock, weight='wnoz', dir_save=ran_save_dir, ffname=ffname)


