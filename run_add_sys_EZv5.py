from add_sys_EZv5 import calc_wcp_fc_wnoz_sec
import sys
from nbodykit.source.catalog import FITSCatalog
import datetime

ihemi = sys.argv[1]
istart = int(sys.argv[2])
iend = int(sys.argv[3])

ezv5dir_w = '/global/cscratch1/sd/octobers/Projects/Simulation/EZmock/QSO_v5_wcp_wnoz/'
for imock in range(istart, iend):
    datv5 = FITSCatalog(os.path.join(ezv5dir, '%s/EZmock_eBOSS_QSO_%s_v5_%04d.fits.gz'%(ihemi.upper(), ihemi.upper(), imock)))
    outfile = os.path.join(ezv5dir_w, '%s/EZmock_eBOSS_QSO_%s_v5_sec_fc_%04d.fits'%(ihemi.upper(), ihemi.upper(), imock))
    try:
        im2file = 'im2_sec_frac_qso_v5_%s.txt'%(ihemi.lower())
        aa = calc_wcp_fc_wnoz_sec(imock, datv5, im2file, outfile)
        if np.mod(imock-1, 20)==0: print('save file to', outfile)
    except:
        print('fail to run calc_wcp_wnoz for imock =, ', imock, ', try to remove RA=180')
        edge_idx = np.where(datv5['RA'] == 180)[0]
        if len(edge_idx) > 0:
            arr = datv5['RA'].compute()
            arr[edge_idx] = 180.00001
            datv5['RA'] = arr
            print(np.where(datv5['RA'])==180)
            calc_wcp_fc_wnoz_sec(imock, datv5, outfile)
    print ('done!')
    print(datetime.datetime.now())

