#!/bin/bash
#SBATCH -o jobs/tjob.out.%j
#SBATCH -e jobs/tjob.err.%j
#SBATCH --qos=debug
#SBATCH --nodes=4
#SBATCH --constraint=knl
#SBATCH --time=00:15:00
#SBATCH --array=51-52

echo 'imock=', $SLURM_ARRAY_TASK_ID

source activate nbodykit-env3

srun -n 64 python run_add_sys_EZv6d1_parallel.py ngc $SLURM_ARRAY_TASK_ID
