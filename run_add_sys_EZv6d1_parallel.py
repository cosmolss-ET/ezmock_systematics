from add_sys_EZv6d1 import calc_wcp_fc_wnoz_sec
import sys
import os
import numpy as np
from nbodykit.source.catalog import FITSCatalog
import datetime

ihemi = sys.argv[1]
imock = int(sys.argv[2])

ezv5dir = '/global/cscratch1/sd/octobers/Projects/Simulation/EZmock/QSO_v6_1/'
ezv5dir_w = '/global/project/projectdirs/eboss/octobers/Simulations/EZmocks/QSO_v6_1_wcp_wnoz/' 
datv6d1 = FITSCatalog(os.path.join(ezv5dir, '%s/EZmock_eBOSS_QSO_%s_v6_1_%04d.fits'%(ihemi.upper(), ihemi.upper(), imock)))
outfile = os.path.join(ezv5dir_w, '%s/EZmock_eBOSS_QSO_%s_v6_1_sec_fc_%04d.fits'%(ihemi.upper(), ihemi.upper(), imock))
try:
    im2file = 'im2_sec_frac_qso_v5_%s.txt'%(ihemi.lower())
    aa = calc_wcp_fc_wnoz_sec(imock, datv6d1, im2file, outfile)
    if np.mod(imock-1, 20)==0: print('save file to', outfile)
except:
    print('fail to run calc_wcp_wnoz for imock =, ', imock, ', try to remove RA=180')
    edge_idx = np.where(datv6d1['RA'] == 180)[0]
    if len(edge_idx) > 0:
        arr = datv6d1['RA'].compute()
        arr[edge_idx] = 180.00001
        datv6d1['RA'] = arr
        print(np.where(datv6d1['RA'])==180)
        calc_wcp_fc_wnoz_sec(imock, datv6d1, outfile)
print ('done!')
print(datetime.datetime.now())

